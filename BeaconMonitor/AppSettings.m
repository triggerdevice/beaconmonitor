//
//  AppSettings.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/23.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "MeshbluDevice.h"
#import "BeaconSettings.h"
#import "CLBeaconRegion+Convenience.h"
#import "AppSettings.h"

static NSString * const kEnableTransmissionKey = @"enable_transmission";
static NSString * const kHostKey = @"host";
static NSString * const kBeaconCommonUUIDKey = @"ibeacon_common_uuid";

static NSString * const kRegion1MajorKey = @"region-1_major";
static NSString * const kRegion1MinorKey = @"region-1_minor";
static NSString * const kRegion1NotificationTriggerKey = @"region-1_notification_trigger";
static NSString * const kRegion1DestinationMeshbliTriggerKey = @"region-1_destination_meshblu_trigger";

static NSString * const kRegion2MajorKey = @"region-2_major";
static NSString * const kRegion2MinorKey = @"region-2_minor";
static NSString * const kRegion2NotificationTriggerKey = @"region-2_notification_trigger";
static NSString * const kRegion2DestinationMeshbliTriggerKey = @"region-2_destination_meshblu_trigger";

static NSString * const kRegion3MajorKey = @"region-3_major";
static NSString * const kRegion3MinorKey = @"region-3_minor";
static NSString * const kRegion3NotificationTriggerKey = @"region-3_notification_trigger";
static NSString * const kRegion3DestinationMeshbliTriggerKey = @"region-3_destination_meshblu_trigger";

static NSString * const kRegion4MajorKey = @"region-4_major";
static NSString * const kRegion4MinorKey = @"region-4_minor";
static NSString * const kRegion4NotificationTriggerKey = @"region-4_notification_trigger";
static NSString * const kRegion4DestinationMeshbliTriggerKey = @"region-4_destination_meshblu_trigger";

static NSString * const kRegion5MajorKey = @"region-5_major";
static NSString * const kRegion5MinorKey = @"region-5_minor";
static NSString * const kRegion5NotificationTriggerKey = @"region-5_notification_trigger";
static NSString * const kRegion5DestinationMeshbliTriggerKey = @"region-5_destination_meshblu_trigger";

static NSString * const kMeshbluTrigger1UUID = @"meshblu_trigger-1_uuid";
static NSString * const kMeshbluTrigger1Token = @"meshblu_trigger-1_token";
static NSString * const kMeshbluTrigger2UUID = @"meshblu_trigger-2_uuid";
static NSString * const kMeshbluTrigger2Token = @"meshblu_trigger-2_token";
static NSString * const kMeshbluTrigger3UUID = @"meshblu_trigger-3_uuid";
static NSString * const kMeshbluTrigger3Token = @"meshblu_trigger-3_token";
static NSString * const kMeshbluTrigger4UUID = @"meshblu_trigger-4_uuid";
static NSString * const kMeshbluTrigger4Token = @"meshblu_trigger-4_token";
static NSString * const kMeshbluTrigger5UUID = @"meshblu_trigger-5_uuid";
static NSString * const kMeshbluTrigger5Token = @"meshblu_trigger-5_token";


@interface AppSettings ()
@property (nonatomic, strong) NSUserDefaults *userDefaults;
@end

@implementation AppSettings

+ (NSDictionary *)settingsDictionaryWithContentsOfURL:(NSURL *)url
{
  NSString *jsonString = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:NULL];
  NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
  
  NSError *error = nil;
  NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];

  if (error) {
    return nil;
  }
  return dic;
}

+ (void)initialize
{
  NSURL *settingsURL = [[NSBundle mainBundle] URLForResource:@"RegisterDefaults" withExtension:@"json"];
  NSDictionary *defaults = [self settingsDictionaryWithContentsOfURL:settingsURL];

  [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
}

- (instancetype)init
{
  self = [super init];
  if (self) {
    _userDefaults = [NSUserDefaults standardUserDefaults];
  }
  return self;
}

#pragma mark - Accessor

- (BOOL)isTransmissionEnabled
{
  return [_userDefaults boolForKey:kEnableTransmissionKey];
}

- (NSString *)host
{
  return [_userDefaults stringForKey:kHostKey];
}

#pragma mark - iBeacon

- (NSString *)beaconCommonUUID
{
  return [_userDefaults stringForKey:kBeaconCommonUUIDKey];
}

#pragma mark -

- (NSArray *)allBeaconSettings
{
  return @[[self beaconSettings1],
           [self beaconSettings2],
           [self beaconSettings3],
           [self beaconSettings4],
           [self beaconSettings5],
           ];
}

- (NSArray *)allMeshbluTriggers
{
  return @[[self meshbluTrigger1],
           [self meshbluTrigger2],
           [self meshbluTrigger3],
           [self meshbluTrigger4],
           [self meshbluTrigger5],
           ];
}

// trigger-?
// ?: index
- (MeshbluTrigger *)findMeshbluTriggerWithTriggerIndex:(NSUInteger)index
{
  NSArray *meshbluDevices = [self allMeshbluTriggers];

  if (index == 0 || meshbluDevices.count < index) {
    return nil;
  }
  return meshbluDevices[index-1];
}

- (BeaconSettings *)beaconSettings1
{
  NSUInteger triggerIndex = [_userDefaults integerForKey:kRegion1DestinationMeshbliTriggerKey];

  return [[BeaconSettings alloc] initWithUUID:[self beaconCommonUUID]
                                        Major:[self p_numberForKey:kRegion1MajorKey]
                                        minor:[self p_numberForKey:kRegion1MinorKey]
                          notificationTrigger:[_userDefaults integerForKey:kRegion1NotificationTriggerKey]
                               meshbluTrigger:[self findMeshbluTriggerWithTriggerIndex:triggerIndex]];
}

- (BeaconSettings *)beaconSettings2
{
  NSUInteger triggerIndex = [_userDefaults integerForKey:kRegion2DestinationMeshbliTriggerKey];

  return [[BeaconSettings alloc] initWithUUID:[self beaconCommonUUID]
                                        Major:[self p_numberForKey:kRegion2MajorKey]
                                        minor:[self p_numberForKey:kRegion2MinorKey]
                          notificationTrigger:[_userDefaults integerForKey:kRegion2NotificationTriggerKey]
                               meshbluTrigger:[self findMeshbluTriggerWithTriggerIndex:triggerIndex]];
}

- (BeaconSettings *)beaconSettings3
{
  NSUInteger triggerIndex = [_userDefaults integerForKey:kRegion3DestinationMeshbliTriggerKey];

  return [[BeaconSettings alloc] initWithUUID:[self beaconCommonUUID]
                                        Major:[self p_numberForKey:kRegion3MajorKey]
                                        minor:[self p_numberForKey:kRegion3MinorKey]
                          notificationTrigger:[_userDefaults integerForKey:kRegion3NotificationTriggerKey]
                               meshbluTrigger:[self findMeshbluTriggerWithTriggerIndex:triggerIndex]];
}

- (BeaconSettings *)beaconSettings4
{
  NSUInteger triggerIndex = [_userDefaults integerForKey:kRegion4DestinationMeshbliTriggerKey];

  return [[BeaconSettings alloc] initWithUUID:[self beaconCommonUUID]
                                        Major:[self p_numberForKey:kRegion4MajorKey]
                                        minor:[self p_numberForKey:kRegion4MinorKey]
                          notificationTrigger:[_userDefaults integerForKey:kRegion4NotificationTriggerKey]
                               meshbluTrigger:[self findMeshbluTriggerWithTriggerIndex:triggerIndex]];
}

- (BeaconSettings *)beaconSettings5
{
  NSUInteger triggerIndex = [_userDefaults integerForKey:kRegion5DestinationMeshbliTriggerKey];

  return [[BeaconSettings alloc] initWithUUID:[self beaconCommonUUID]
                                        Major:[self p_numberForKey:kRegion5MajorKey]
                                        minor:[self p_numberForKey:kRegion5MinorKey]
                          notificationTrigger:[_userDefaults integerForKey:kRegion5NotificationTriggerKey]
                               meshbluTrigger:[self findMeshbluTriggerWithTriggerIndex:triggerIndex]];
}

- (MeshbluTrigger *)meshbluTrigger1
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger1UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger1Token];
  
  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (MeshbluTrigger *)meshbluTrigger2
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger2UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger2Token];

  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (MeshbluTrigger *)meshbluTrigger3
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger3UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger3Token];

  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (MeshbluTrigger *)meshbluTrigger4
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger4UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger4Token];

  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (MeshbluTrigger *)meshbluTrigger5
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger5UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger5Token];

  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (BeaconSettings *)findBeaconSettingsWithBeaconRegion:(CLBeaconRegion *)beaconRegion
{
  __block BeaconSettings *foundSettings = nil;

  [[self allBeaconSettings] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
    BeaconSettings *settings = obj;
    if ([settings.region isEqual:beaconRegion]) {
      foundSettings = settings;
      *stop = YES;
      return;
    }
  }];

  NSLog(@"entry:%d, exit:%d", foundSettings.region.notifyOnEntry, foundSettings.region.notifyOnExit);
  return foundSettings;
}

- (NSArray *)allBeaconRegions
{
  NSMutableArray *regions = [NSMutableArray array];

  CLBeaconRegion *region;
  if ((region = self.beaconSettings1.region)) {
    [regions addObject:region];
  }
  if ((region = self.beaconSettings2.region)) {
    [regions addObject:region];
  }
  if ((region = self.beaconSettings3.region)) {
    [regions addObject:region];
  }
  if ((region = self.beaconSettings4.region)) {
    [regions addObject:region];
  }
  if ((region = self.beaconSettings5.region)) {
    [regions addObject:region];
  }
  return [regions copy];
}
#pragma mark -

- (NSNumber *)p_numberForKey:(NSString *)key
{
  NSString *value = [_userDefaults stringForKey:key];
  if ([self p_isEmptyString:value]) {
    return nil;
  }
  return @([value integerValue]);
}


- (BOOL)p_isEmptyString:(NSString *)string
{
  return (!string || [string isEqualToString:@""]);
}

@end
