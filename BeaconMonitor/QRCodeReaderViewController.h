//
//  QRCodeReaderViewController.h
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/27.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QRCodeReaderViewController;

@protocol QRCodeReaderViewControllerDelegate <NSObject>
- (void)qrCodeReaderDidTapCloseButton:(QRCodeReaderViewController *)controller;
- (void)qrCodeReaderDidTapImportButton:(QRCodeReaderViewController *)controller;
@end

@interface QRCodeReaderViewController : UIViewController

@property (nonatomic, weak) id<QRCodeReaderViewControllerDelegate> delegate;
@property (readonly, nonatomic, copy) NSString *detectString;

@end
