//
//  CLBeaconRegion+Convenience.h
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/24.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "BeaconSettings.h"

@interface CLBeaconRegion (Convenience)

+ (instancetype)beaconRegionWithUUIDString:(NSString *)aUUIDString major:(NSNumber *)major minor:(NSNumber *)minor;
+ (instancetype)beaconRegionWithUUIDString:(NSString *)aUUIDString major:(NSNumber *)major minor:(NSNumber *)minor notificationTrigger:(BeaconSettingsNotificationTrigger)notificationTrigger;

@end
