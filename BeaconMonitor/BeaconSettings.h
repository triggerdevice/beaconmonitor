//
//  BeaconSettings.h
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/27.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, BeaconSettingsNotificationTrigger)
{
  BeaconSettingsNotificationTriggerOnEnter = 1 << 0,
  BeaconSettingsNotificationTriggerOnExit = 1 << 1,
  BeaconSettingsNotificationTriggerBoth = BeaconSettingsNotificationTriggerOnEnter | BeaconSettingsNotificationTriggerOnExit,
};

@class CLBeaconRegion, MeshbluTrigger;

@interface BeaconSettings : NSObject
@property (readonly, nonatomic, strong) CLBeaconRegion *region;

@property (readonly, nonatomic, strong) NSNumber *major;
@property (readonly, nonatomic, strong) NSNumber *minor;
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger;

- (instancetype)initWithUUID:(NSString *)UUIDString Major:(NSNumber *)major minor:(NSNumber *)minor notificationTrigger:(BeaconSettingsNotificationTrigger)notificationTrigger meshbluTrigger:(MeshbluTrigger *)meshbluTrigger;

@end
