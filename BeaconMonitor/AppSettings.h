//
//  AppSettings.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/23.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CLBeaconRegion;
@class MeshbluTrigger;
@class BeaconSettings;

@interface AppSettings : NSObject

@property (readonly, nonatomic, assign) BOOL isTransmissionEnabled;
@property (readonly, nonatomic, copy) NSString *host;
@property (readonly, nonatomic, copy) NSString *beaconCommonUUID;

@property (readonly, nonatomic, strong) BeaconSettings *beaconSettings1;
@property (readonly, nonatomic, strong) BeaconSettings *beaconSettings2;
@property (readonly, nonatomic, strong) BeaconSettings *beaconSettings3;
@property (readonly, nonatomic, strong) BeaconSettings *beaconSettings4;
@property (readonly, nonatomic, strong) BeaconSettings *beaconSettings5;

@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger1;
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger2;
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger3;
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger4;
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger5;

/// CLBeaconRegionオブジェクトから紐付いたBeaconSettingsオブジェクトを取得する
- (BeaconSettings *)findBeaconSettingsWithBeaconRegion:(CLBeaconRegion *)beaconRegion;

- (NSArray *)allMeshbluTriggers;
- (NSArray *)allBeaconSettings;
- (NSArray *)allBeaconRegions;
@end
