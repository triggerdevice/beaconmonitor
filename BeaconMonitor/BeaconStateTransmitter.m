//
//  BeaconStateTransmitter.m
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/24.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

@import CoreLocation;
#import "BeaconStateTransmitter.h"

static NSString * const kPayloadUUIDKey = @"uuid";
static NSString * const kPayloadMajorKey = @"major";
static NSString * const kPayloadMinorKey = @"minor";
static NSString * const kPayloadStateKey = @"state";
static NSString * const kPayloadStateInsideValue = @"inside";
static NSString * const kPayloadStateOutValue = @"outside";


@implementation BeaconStateTransmitter

- (instancetype)initWithHost:(NSString *)host
{
  return [super initWithHost:host triggerUUID:nil triggerToken:nil devices:nil];
}

- (void)postTriggerWithTriggerUUID:(NSString *)triggerUUIDString token:(NSString *)triggerTokenString success:(void (^)(void))successBlock failure:(void (^)(NSError *error))failureBlock
{
  self.triggerUUID = triggerUUIDString;
  self.triggerToken = triggerTokenString;

  [self httpPostTrigger:successBlock failure:failureBlock];
}

- (void)postEnterStateWithRegion:(CLBeaconRegion *)beaconRegion triggerUUID:(NSString *)triggerUUIDString token:(NSString *)triggerTokenString success:(void (^)(void))successBlock failure:(void (^)(NSError *error))failureBlock
{
  [self p_postState:kPayloadStateInsideValue
             region:beaconRegion
        triggerUUID:triggerUUIDString
              token:triggerTokenString
            success:successBlock
            failure:failureBlock];
}

- (void)postExitStateWithRegion:(CLBeaconRegion *)beaconRegion triggerUUID:(NSString *)triggerUUIDString token:(NSString *)triggerTokenString success:(void (^)(void))successBlock failure:(void (^)(NSError *))failureBlock
{
  [self p_postState:kPayloadStateOutValue
             region:beaconRegion
        triggerUUID:triggerUUIDString
              token:triggerTokenString
            success:successBlock
            failure:failureBlock];
}

#pragma mark - Private instance methods

- (void)p_postState:(NSString *)stateString region:(CLBeaconRegion *)beaconRegion triggerUUID:(NSString *)triggerUUIDString token:(NSString *)triggerTokenString success:(void (^)(void))successBlock failure:(void (^)(NSError *))failureBlock
{
  self.triggerUUID = triggerUUIDString;
  self.triggerToken = triggerTokenString;
  
  NSMutableDictionary *payload = [NSMutableDictionary dictionaryWithDictionary:[self p_payloadWithbeaconRegion:beaconRegion]];
  [payload setObject:stateString forKey:kPayloadStateKey];
  
  [self httpPostValues:[payload copy] success:successBlock failure:failureBlock];
}

- (NSDictionary *)p_payloadWithbeaconRegion:(CLBeaconRegion *)region
{
  // UUID, major, minor, inside/outside
  NSMutableDictionary *payload = [NSMutableDictionary dictionary];
  
  [payload setObject:region.proximityUUID.UUIDString forKey:kPayloadUUIDKey];
  if (region.major) {
    [payload setObject:region.major forKey:kPayloadMajorKey];
  }
  if (region.minor) {
    [payload setObject:region.minor forKey:kPayloadMinorKey];
  }
  
  return [payload copy];
}

@end
