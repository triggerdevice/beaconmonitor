//
//  BeaconStatusLogTableViewController.m
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/24.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

@import CoreLocation;

#import "IASKAppSettingsViewController.h"
#import "QRCodeReaderViewController.h"

#import "AppDelegate.h"
#import "AppSettings.h"
#import "AppSettings+Import.h"
#import "BeaconSettings.h"
#import "MeshbluDevice.h"

#import "BeaconMonitor.h"
#import "CLBeaconRegion+Convenience.h"

#import "BeaconStateTransmitter.h"
#import "BeaconRegionInfo.h"
#import "BeaconStatusLogTableViewController.h"

@interface BeaconStatusLogTableViewController () <BeaconMonitorDelegate, IASKSettingsDelegate, QRCodeReaderViewControllerDelegate>
@property (nonatomic, weak) AppSettings *appSettings;
@property (nonatomic, strong) BeaconMonitor *beaconMonitor;
@property (nonatomic, strong) BeaconStateTransmitter *transmitter;

@property NSMutableArray *objects;  // TableViewに表示するBeaconRegionInfoオブジェクトのコンテナ

// InAppSettingsKit
@property (nonatomic, strong) IASKAppSettingsViewController *appSettingsViewController;

// Outlets & Actions
- (IBAction)settingsButtonDidPush:(id)sender;
@end

@implementation BeaconStatusLogTableViewController

- (void)awakeFromNib
{
  [super awakeFromNib];

  _objects = [NSMutableArray array];
  
  AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
  _appSettings = appDelegate.appSettings;

  _beaconMonitor = [[BeaconMonitor alloc] init];
  _beaconMonitor.delegate = self;

  _transmitter = [[BeaconStateTransmitter alloc] initWithHost:_appSettings.host];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.

  [self p_restartMonitoringUsingAppSettings];

}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - accessor

- (IASKAppSettingsViewController *)appSettingsViewController
{
  if (!_appSettingsViewController) {
    _appSettingsViewController = [[IASKAppSettingsViewController alloc] init];
    _appSettingsViewController.delegate = self;
  }
  return _appSettingsViewController;
}

#pragma mark - Private instance methods

- (void)p_restartMonitoringUsingAppSettings
{
  NSSet *beaconRegions = [NSSet setWithArray:[_appSettings.allBeaconRegions copy]];
  [_beaconMonitor restartMonitoringWithBeaconRegions:beaconRegions];

  // for table view
  [self.objects removeAllObjects];
  for (CLBeaconRegion *region in beaconRegions) {
    [self.objects addObject:[[BeaconRegionInfo alloc] initWithBeaconRegion:region]];
  }
}

- (void)p_onEnterBeaconRegion:(CLBeaconRegion *)beaconRegion
{
  NSLog(@"enter: %@.%@.%@", beaconRegion.proximityUUID.UUIDString, beaconRegion.major, beaconRegion.minor);

  if (_appSettings.isTransmissionEnabled) {
    BeaconSettings *beaconSettings = [_appSettings findBeaconSettingsWithBeaconRegion:beaconRegion];
    if (beaconSettings.region.notifyOnEntry) {
      // NOTE: didDetermineStateを使うのでnotifyOnEntryの状態に関係なく呼ばれるが、値は設定されているので利用する
      [_transmitter postTriggerWithTriggerUUID:beaconSettings.meshbluTrigger.uuid
                                         token:beaconSettings.meshbluTrigger.token
                                       success:nil
                                       failure:^(NSError *error){
                                         NSLog(@"%s, error:%@", __PRETTY_FUNCTION__, [error localizedDescription]);
                                       }];
    }
  }
}

- (void)p_onExitBeaconRegion:(CLBeaconRegion *)beaconRegion
{
  NSLog(@"exit: %@.%@.%@", beaconRegion.proximityUUID.UUIDString, beaconRegion.major, beaconRegion.minor);

  
  if (_appSettings.isTransmissionEnabled) {
    BeaconSettings *beaconSettings = [_appSettings findBeaconSettingsWithBeaconRegion:beaconRegion];
    if (beaconSettings.region.notifyOnExit) {
      // NOTE: didDetermineStateを使うのでnotifyOnExitの状態に関係なく呼ばれるが、値は設定されているので利用する
      [_transmitter postTriggerWithTriggerUUID:beaconSettings.meshbluTrigger.uuid
                                         token:beaconSettings.meshbluTrigger.token
                                       success:nil
                                       failure:^(NSError *error){
                                         NSLog(@"%s, error:%@", __PRETTY_FUNCTION__, [error localizedDescription]);
                                       }];
    }
  }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return _objects.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
  return (section == 0) ? _appSettings.beaconCommonUUID : nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

  UILabel *titleLabel = (UILabel *)[cell viewWithTag:1];
  UILabel *subTitleLabel = (UILabel *)[cell viewWithTag:2];
  
  BeaconRegionInfo *beaconRegionInfo = self.objects[indexPath.row];
  titleLabel.text = beaconRegionInfo.title;
  subTitleLabel.text = beaconRegionInfo.subTitle;

  return cell;
}

#pragma mark - <BeaconMonitorDelegate>

/// ビーコン領域への侵入状態が決まった(変化した)
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor didDetermineState:(CLRegionState)state forBeaconRegion:(CLBeaconRegion *)beaconRegion
{
  [self.objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    BeaconRegionInfo *info = obj;
    if (![info isEqualToBeaconRegion:beaconRegion]) {
      return;
    }
    info.state = state;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]]
                          withRowAnimation:UITableViewRowAnimationNone];
  }];
  
  if (state == CLRegionStateInside) {
    [self p_onEnterBeaconRegion:beaconRegion];
  }
  else if (state == CLRegionStateOutside) {
    [self p_onExitBeaconRegion:beaconRegion];
  }
}

/// ビーコン領域に入った
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor didEnterBeaconRegion:(CLBeaconRegion *)beaconRegion
{
}

/// ビーコン領域から出た
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor didExitBeaconRegion:(CLBeaconRegion *)beaconRegion
{
}

/// ビーコン領域の領域監視を開始した
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor didStartMonitoringForRegion:(CLBeaconRegion *)beaconRegion
{
  NSLog(@"%s, %@", __PRETTY_FUNCTION__, beaconRegion);
}

/// ビーコン領域の領域監視に失敗した
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor monitoringDidFailForRegion:(CLBeaconRegion *)beaconRegion withError:(NSError *)error
{
  NSLog(@"%s, %@", __PRETTY_FUNCTION__, [error localizedDescription]);
}

#pragma mark - <IASKSettingsDelegate>

- (void)settingsViewControllerDidEnd:(IASKAppSettingsViewController *)sender
{
  [sender dismissViewControllerAnimated:YES completion:nil];
  
  // 設定を反映
  _transmitter.host = _appSettings.host;

  // 設定変更の有無にかかわらず領域監視をリスタートする
  [self p_restartMonitoringUsingAppSettings];

  [self.tableView reloadData];
}

- (void)settingsViewController:(IASKAppSettingsViewController *)sender buttonTappedForSpecifier:(IASKSpecifier *)specifier
{
  if ([specifier.key isEqualToString:@"QRCodeReaderLaunchAction"]) {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"QRCodeReader"];
    QRCodeReaderViewController *vc = (QRCodeReaderViewController *)navigationController.topViewController;
    NSParameterAssert([vc isKindOfClass:[QRCodeReaderViewController class]]);
    vc.delegate = self;
    
    [sender presentViewController:navigationController animated:YES completion:nil];

  }
  
}

#pragma mark - <QRCodeReaderViewControllerDelegate>

- (void)qrCodeReaderDidTapCloseButton:(QRCodeReaderViewController *)controller
{
  [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)qrCodeReaderDidTapImportButton:(QRCodeReaderViewController *)controller
{
  NSLog(@"%@", controller.detectString);
  [AppSettings importMeshbluDevicesFromJSONString:controller.detectString];
  [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Actions

- (IBAction)settingsButtonDidPush:(id)sender
{
  UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.appSettingsViewController];
  self.appSettingsViewController.showDoneButton = YES;
  
  [self presentViewController:navigationController animated:YES completion:nil];
}

@end
