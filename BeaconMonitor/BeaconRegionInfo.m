//
//  BeaconRegionInfo.m
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/25.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

@import CoreLocation;
#import "BeaconRegionInfo.h"

@interface BeaconRegionInfo ()
@property (nonatomic, strong) CLBeaconRegion *beaconRegion;
@end

@implementation BeaconRegionInfo

- (instancetype)initWithBeaconRegion:(CLBeaconRegion *)beaconRegion
{
  NSParameterAssert([beaconRegion isKindOfClass:[CLBeaconRegion class]]);

  self = [super init];
  if (self) {
    _beaconRegion = beaconRegion;
    _state = CLRegionStateUnknown;
  }
  return self;
}

- (void)setState:(NSUInteger)state
{
  NSParameterAssert(state == CLRegionStateUnknown || state == CLRegionStateInside || state == CLRegionStateOutside);

  if (_state == state) {
    return;
  }
  _state = state;
}

- (NSString *)title
{
  return [NSString stringWithFormat:@"major: %@, minor: %@",
          _beaconRegion.major ? _beaconRegion.major : @"*",
          _beaconRegion.minor ? _beaconRegion.minor : @"*"];
}

- (NSString *)subTitle
{
  NSArray *stateString = @[@"Unknown", @"Inside", @"Outside"];
  return [NSString stringWithFormat:@"state: %@", stateString[_state]];
}

- (BOOL)isEqualToBeaconRegion:(CLBeaconRegion *)beaconRegion
{
  return [_beaconRegion isEqual:beaconRegion];
}

@end
