//
//  CLBeaconRegion+Convenience
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/24.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "CLBeaconRegion+Convenience.h"

@implementation CLBeaconRegion (Convenience)

+ (instancetype)beaconRegionWithUUIDString:(NSString *)aUUIDString major:(NSNumber *)major minor:(NSNumber *)minor
{
  return [self beaconRegionWithUUIDString:aUUIDString
                                    major:major
                                    minor:minor
                      notificationTrigger:BeaconSettingsNotificationTriggerBoth];
}

+ (instancetype)beaconRegionWithUUIDString:(NSString *)aUUIDString major:(NSNumber *)major minor:(NSNumber *)minor notificationTrigger:(BeaconSettingsNotificationTrigger)notificationTrigger
{
  if (!aUUIDString) {
    // UUIDがnilだと00000000-0000-0000-0000-000000000000扱いになるがエラーとしておく
    return nil;
  }
  
  NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:aUUIDString];
  NSMutableString *baseIdentifier = [[[NSBundle mainBundle] bundleIdentifier] mutableCopy];
  
  CLBeaconRegion *beaconRegion = nil;
  if (proximityUUID && major && minor) {
    [baseIdentifier appendFormat:@".%@.%@.%@", aUUIDString, major, minor];
    beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID
                                                           major:major.intValue
                                                           minor:minor.intValue
                                                      identifier:[baseIdentifier copy]];
  }
  else if (proximityUUID && major && !minor) {
    [baseIdentifier appendFormat:@".%@.%@", aUUIDString, major];
    beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID
                                                           major:major.intValue
                                                      identifier:[baseIdentifier copy]];
  }
  else if (proximityUUID && !major && !minor) {
    [baseIdentifier appendFormat:@".%@", aUUIDString];
    beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID
                                                      identifier:[baseIdentifier copy]];
  }
  else {
    return nil;
  }
  beaconRegion.notifyOnEntry = ((notificationTrigger & BeaconSettingsNotificationTriggerOnEnter) == BeaconSettingsNotificationTriggerOnEnter);
  beaconRegion.notifyOnExit = ((notificationTrigger & BeaconSettingsNotificationTriggerOnExit) == BeaconSettingsNotificationTriggerOnExit);

  return beaconRegion;
}
@end
