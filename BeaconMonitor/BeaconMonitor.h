//
//  BeaconMonitor.h
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/24.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BeaconMonitor;

@protocol BeaconMonitorDelegate <NSObject>
@optional

/// ビーコン領域への侵入状態が決まった(変化した)
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor didDetermineState:(CLRegionState)state forBeaconRegion:(CLBeaconRegion *)beaconRegion;

/// ビーコン領域に入った
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor didEnterBeaconRegion:(CLBeaconRegion *)beaconRegion;

/// ビーコン領域から出た
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor didExitBeaconRegion:(CLBeaconRegion *)beaconRegion;

/// ビーコン領域の領域監視を開始した
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor didStartMonitoringForRegion:(CLBeaconRegion *)beaconRegion;

/// ビーコン領域の領域監視に失敗した
- (void)beaconMonitor:(BeaconMonitor *)beaconMonitor monitoringDidFailForRegion:(CLBeaconRegion *)beaconRegion withError:(NSError *)error;
@end


@interface BeaconMonitor : NSObject

@property (nonatomic, weak) id<BeaconMonitorDelegate> delegate;
@property(readonly, nonatomic, copy) NSSet *monitoredRegions;

/**
 ビーコン領域情報を指定して領域監視を開始する
 @param aUUIDString 監視するビーコン領域のUUID文字列
 @param major 監視するビーコン領域のmajor番号。nilを指定するとmajor番号で監視しない。
 @param minor 監視するビーコン領域のminor番号。nilを指定するとminor番号で監視しない。
 @warning 引数の指定方法はuuidのみ、uuid+major, uuid+major+minorの３種。それ以外は領域監視不可。
 */
- (void)startMonitoringWithUUIDString:(NSString *)aUUIDString major:(NSNumber *)major minor:(NSNumber *)minor;

/**
 ビーコン領域情報を指定して領域監視を開始する
 @param aUUIDString 監視するビーコン領域のUUID文字列
 @param major 監視するビーコン領域のmajor番号。nilを指定するとmajor番号で監視しない。
 @param minor 監視するビーコン領域のminor番号。nilを指定するとminor番号で監視しない。
 @param notifyOnEntry YES:ビーコン領域に入ったときに通知する
 @param notifyOnExit YES:ビーコン領域から出たときに通知する
 @warning 引数の指定方法はuuidのみ、uuid+major, uuid+major+minorの３種。それ以外は領域監視不可。
 */
- (void)startMonitoringWithUUIDString:(NSString *)aUUIDString major:(NSNumber *)major minor:(NSNumber *)minor notifyOnEntry:(BOOL)notifyOnEntry notifyOnExit:(BOOL)notifyOnExit;

/**
 複数ビーコン領域を一括登録して領域監視を開始する
 @param beaconRegions ビーコン領域(CLBeaconRegionオブジェクト)のセット
 */
- (void)startMonitoringWithBeaconRegions:(NSSet *)beaconRegions;

/**
 すでに登録されているビーコン領域監視をすべて停止してから、複数ビーコン領域を一括登録して領域監視を開始する
 @param beaconRegions ビーコン領域(CLBeaconRegionオブジェクト)のセット
 */
- (void)restartMonitoringWithBeaconRegions:(NSSet *)beaconRegions;

/**
 指定したビーコン領域の監視を停止する
 @param beaconRegion 領域監視を停止したいビーコン領域
 */
- (void)stopMonitoringForBeaconRegion:(CLBeaconRegion *)beaconRegion;

/**
 すべてのビーコン領域の監視を停止する
 */
- (void)stopAllMonitoring;

@end
