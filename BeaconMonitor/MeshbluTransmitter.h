//
//  MeshbluTransmitter.h
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/24.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

// required: AFNetworking


@interface MeshbluTransmitter : NSObject

/// 接続するサーバー
@property (nonatomic, copy) NSString *host;

/// メッセージを送信するtriggerのUUID
@property (nonatomic, copy) NSString *triggerUUID;

/// メッセージを送信するtriggerのtoken
@property (nonatomic, copy) NSString *triggerToken;

/// メッセージを転送するactionのUUID文字列
@property (nonatomic, copy) NSArray *devices;

/**
 サーバー情報を指定して初期化する
 @param host 送信先ホスト名
 @param triggerUUID   送るトリガーのUUID
 @param triggerToken  送るトリガーのtoken
 @param devices 宛先デバイスのUUID文字列の配列
 @return instancetype 初期化されたインスタンス
 */
- (instancetype)initWithHost:(NSString *)host triggerUUID:(NSString *)triggerUUID triggerToken:(NSString *)triggerToken devices:(NSArray *)devices;

/**
 HTTP通信でMeshbluサーバーにデータをPOSTする
 @param values payloadに相当するkey-value情報
 @param successBlock 送信が成功した場合に呼ばれるブロック
 @param failureBlock 送信が失敗した場合に呼ばれるブロック。エラー原因はerror引数に格納される。
 */
- (void)httpPostValues:(NSDictionary *)values success:(void (^)(void))successBlock failure:(void (^)(NSError *error))failureBlock;

/**
 HTTPS通信でMeshbluのtriggerを発火する
 @param successBlock 送信が成功した場合に呼ばれるブロック
 @param failureBlock 送信が失敗した場合に呼ばれるブロック。エラー原因はerror引数に格納される。
 */
- (void)httpPostTrigger:(void (^)(void))successBlock failure:(void (^)(NSError *error))failureBlock;
@end
