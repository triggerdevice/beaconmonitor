//
//  BeaconStateTransmitter.h
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/24.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "MeshbluTransmitter.h"

@class CLBeaconRegion;

@interface BeaconStateTransmitter : MeshbluTransmitter

/**
 host情報を指定してインスタンスを初期化する
 @param host 接続するhttpsホスト(IPアドレスまたはホスト名)
 */
- (instancetype)initWithHost:(NSString *)host;

/**
 ビーコン領域の侵入ステータスが変化したときにサーバーに通知する(発火する)
 @param triggerUUIDString 発火させるtriggerのUUID
 @param triggerTokenString 発火させるtriggerのtoken
 @param successBlock 送信が成功した場合に呼ばれるブロック
 @param failureBlock 送信が失敗した場合に呼ばれるブロック。エラー原因はerror引数に格納される。
 */
- (void)postTriggerWithTriggerUUID:(NSString *)triggerUUIDString token:(NSString *)triggerTokenString success:(void (^)(void))successBlock failure:(void (^)(NSError *error))failureBlock;

/**
 ビーコン領域に侵入したことをサーバーに通知する
 @param beaconRegion 侵入したビーコン領域
 @param triggerUUIDString メッセージ送信先のtrigger UUID
 @param triggerTokenString メッセージ送信先のtrigger Token
 @param successBlock 送信が成功した場合に呼ばれるブロック
 @param failureBlock 送信が失敗した場合に呼ばれるブロック。エラー原因はerror引数に格納される。
 @note "payload"にはuuid, major, minor, state("inside")情報が含まれる。対象actionは本API呼び出し前にdevicesプロパティに指定しておくこと。
 */
- (void)postEnterStateWithRegion:(CLBeaconRegion *)beaconRegion triggerUUID:(NSString *)triggerUUIDString token:(NSString *)triggerTokenString success:(void (^)(void))successBlock failure:(void (^)(NSError *error))failureBlock;

/**
 ビーコン領域から退出したことをサーバーに通知する
 @param beaconRegion 退出したビーコン領域
 @param triggerUUIDString メッセージ送信先のtrigger UUID
 @param triggerTokenString メッセージ送信先のtrigger Token
 @param successBlock 送信が成功した場合に呼ばれるブロック
 @param failureBlock 送信が失敗した場合に呼ばれるブロック。エラー原因はerror引数に格納される。
 @note "payload"にはuuid, major, minor, state("inside")情報が含まれる。対象actionは本API呼び出し前にdevicesプロパティに指定しておくこと。
 */
- (void)postExitStateWithRegion:(CLBeaconRegion *)beaconRegion triggerUUID:(NSString *)triggerUUIDString token:(NSString *)triggerTokenString success:(void (^)(void))successBlock failure:(void (^)(NSError *))failureBlock;

@end
