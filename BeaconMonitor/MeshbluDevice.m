//
//  MeshbluDevice.m
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/27.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "MeshbluDevice.h"

@interface MeshbluDevice ()
@property (readwrite, nonatomic, copy) NSString *uuid;
@property (readwrite, nonatomic, copy) NSString *token;
@end

@implementation MeshbluDevice
- (instancetype)initWithUUID:(NSString *)uuid token:(NSString *)token
{
  self = [super init];
  if (self) {
    _uuid = uuid;
    _token = token;
  }
  return self;
}
@end

@implementation MeshbluTrigger
@end

@implementation MeshbluAction
@end