//
//  BeaconRegionInfo.h
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/25.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLBeaconRegion;

// TableViewCellで表示するのに使うためのモデル

@interface BeaconRegionInfo : NSObject

@property (readonly, nonatomic, copy) NSString *title;
@property (readonly, nonatomic, copy) NSString *subTitle;

// (CLRegionState)state
@property (nonatomic, assign) NSUInteger state;

- (instancetype)initWithBeaconRegion:(CLBeaconRegion *)beaconRegion;

- (BOOL)isEqualToBeaconRegion:(CLBeaconRegion *)beaconRegion;

@end
