//
//  MeshbluDevice.h
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/27.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MeshbluDevice : NSObject
@property (readonly, nonatomic, copy) NSString *uuid;
@property (readonly, nonatomic, copy) NSString *token;

- (instancetype)initWithUUID:(NSString *)uuid token:(NSString *)token;
@end

@interface MeshbluTrigger : MeshbluDevice
@end

@interface MeshbluAction : MeshbluDevice
@end
