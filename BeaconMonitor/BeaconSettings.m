//
//  BeaconSettings.m
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/27.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "CLBeaconRegion+Convenience.h"
#import "MeshbluDevice.h"
#import "BeaconSettings.h"

@interface BeaconSettings ()
@property (readwrite, nonatomic, strong) CLBeaconRegion *region;
@property (readwrite, nonatomic, strong) MeshbluTrigger *meshbluTrigger;
@end

@implementation BeaconSettings

- (instancetype)initWithUUID:(NSString *)UUIDString Major:(NSNumber *)major minor:(NSNumber *)minor notificationTrigger:(BeaconSettingsNotificationTrigger)notificationTrigger meshbluTrigger:(MeshbluTrigger *)meshbluTrigger
{
  self = [super init];
  if (self) {
    _region = [CLBeaconRegion beaconRegionWithUUIDString:UUIDString major:major minor:minor notificationTrigger:notificationTrigger];
    if (!_region) {
      return nil;
    }
    _meshbluTrigger = meshbluTrigger;
  }
  return self;
}

- (NSNumber *)major
{
  return _region.major;
}

- (NSNumber *)minor
{
  return _region.minor;
}

@end
