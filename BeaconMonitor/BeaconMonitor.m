//
//  BeaconMonitor.m
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/24.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

@import CoreLocation;
#import "CLBeaconRegion+Convenience.h"
#import "BeaconMonitor.h"

@interface BeaconMonitor () <CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation BeaconMonitor

- (instancetype)init
{
  self = [super init];
  if (self) {
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;

    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
      [_locationManager requestAlwaysAuthorization];
    }
  }
  return self;
}

#pragma mark - Public interfaces

- (NSSet *)monitoredRegions
{
  return _locationManager.monitoredRegions;
}

- (void)startMonitoringWithUUIDString:(NSString *)aUUIDString major:(NSNumber *)major minor:(NSNumber *)minor notifyOnEntry:(BOOL)notifyOnEntry notifyOnExit:(BOOL)notifyOnExit
{
  CLBeaconRegion *beaconRegion = [CLBeaconRegion beaconRegionWithUUIDString:aUUIDString
                                                                      major:major
                                                                      minor:minor];
  beaconRegion.notifyOnEntry = notifyOnEntry;
  beaconRegion.notifyOnExit = notifyOnExit;

  if ([CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]]) {
    [_locationManager startMonitoringForRegion:beaconRegion];
  }
  
}

- (void)startMonitoringWithUUIDString:(NSString *)aUUIDString major:(NSNumber *)major minor:(NSNumber *)minor
{
  [self startMonitoringWithUUIDString:aUUIDString
                                major:major
                                minor:minor
                        notifyOnEntry:YES
                         notifyOnExit:YES];
}

- (void)startMonitoringWithBeaconRegions:(NSSet *)beaconRegions;
{
  for (id region in beaconRegions) {
    NSParameterAssert([region isKindOfClass:[CLBeaconRegion class]]);

    CLBeaconRegion *beaconRegion = region;
    [self startMonitoringWithUUIDString:beaconRegion.proximityUUID.UUIDString
                                  major:beaconRegion.major
                                  minor:beaconRegion.minor
                          notifyOnEntry:beaconRegion.notifyOnEntry
                           notifyOnExit:beaconRegion.notifyOnExit];
  }
}

- (void)restartMonitoringWithBeaconRegions:(NSSet *)beaconRegions
{
  [self stopAllMonitoring];
  [self startMonitoringWithBeaconRegions:beaconRegions];
}

- (void)stopMonitoringForBeaconRegion:(CLBeaconRegion *)beaconRegion
{
  NSParameterAssert([beaconRegion isKindOfClass:[CLBeaconRegion class]]);

  [_locationManager stopMonitoringForRegion:beaconRegion];
}

- (void)stopAllMonitoring
{
  for (CLBeaconRegion *region in _locationManager.monitoredRegions) {
    [self stopMonitoringForBeaconRegion:region];
  }
}


#pragma mark - <CLLocationManagerDelegate> (required)

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
  if ([self.delegate respondsToSelector:@selector(beaconMonitor:didDetermineState:forBeaconRegion:)]) {
    [self.delegate beaconMonitor:self
               didDetermineState:state
                 forBeaconRegion:(CLBeaconRegion *)region
     ];
  }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
}

- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
}

- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{
}

#pragma mark - <CLLocationManagerDelegate> (optional)

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
  NSLog(@"%s, status=%d", __PRETTY_FUNCTION__, status);

  if (status == kCLAuthorizationStatusDenied) {
    NSLog(@"アプリの位置情報サービスをONにしてね");
  }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
  if ([self.delegate respondsToSelector:@selector(beaconMonitor:didEnterBeaconRegion:)]) {
    [self.delegate beaconMonitor:self
            didEnterBeaconRegion:(CLBeaconRegion *)region];
  }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
  if ([self.delegate respondsToSelector:@selector(beaconMonitor:didExitBeaconRegion:)]) {
    [self.delegate beaconMonitor:self
             didExitBeaconRegion:(CLBeaconRegion *)region];
  }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
  NSLog(@"%s, %@", __PRETTY_FUNCTION__, [error localizedDescription]);
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
  [manager requestStateForRegion:region];

  if ([self.delegate respondsToSelector:@selector(beaconMonitor:didStartMonitoringForRegion:)]) {
    [self.delegate beaconMonitor:self
             didStartMonitoringForRegion:(CLBeaconRegion *)region];
  }
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
  if ([self.delegate respondsToSelector:@selector(beaconMonitor:monitoringDidFailForRegion:withError:)]) {
    [self.delegate beaconMonitor:self
             monitoringDidFailForRegion:(CLBeaconRegion *)region
                       withError:error];
  }
}

@end
